# coding=utf-8
from django.db import models
from datetime import date
from company_info.models import Team
from tinymce import models as tinymce_models



class CategoryRssNews(models.Model):
    """
    docstring for CategoryRssNews:
    Класс описывает бизнес логику катеогрий новостей отрасли.
    Наличие категории означает, возможность подписки на RSS рассылку.
    """
    name = models.CharField(max_length=250, verbose_name=u'Имя категории (соответствует сайту источнику) '
                                                               u'в ВЕРХНЕМ РЕГИСТРЕ', blank=True, default=u'')
    name_displayed = models.CharField(max_length=250, verbose_name=u'Имя категории '
                                                                   u'(отображаемое на нашем сайте)',
                                      blank=True)
    subscription = models.BooleanField(default=True, verbose_name=u'Категории, на которые подписан RSS')
    visible = models.BooleanField(default=True, verbose_name=u'Видимые на странице "Новости отрасли"')
    # Поле удалено по требованию заказчика (Елисеева)
    #main = models.BooleanField(default=False, verbose_name=u'Присутствие на главной странице (только 2)')
    slug = models.SlugField(default='')

    def get_upper_name(self):
        upper_name = self.name.upper()
        return upper_name

    class Meta:
        verbose_name = u'Категория новостей отрасли'
        verbose_name_plural = u'Категории \n' \
                              u'новотей отрасли'
        ordering = ['id',]

    def related(self):
        "Returns the topics with similar starting names"
        return self._default_manager.filter(name__startswith=self.name)


class RssNews(models.Model):
    """
    docstring for RssNews:
    Класс описывает бизнес логику новостей отрасли на которые оформлена подписка.
    Каждая новость обязана иметь одну категорию.
    """
    class Meta:
        verbose_name = u'Новость отрасли'
        verbose_name_plural = u'Новоти отрасли'
        ordering = ['-event_date',]

    title = models.TextField(verbose_name=u'Заголовок новости', blank=False)
    category_text = models.CharField(max_length=250, verbose_name=u'Ключевые слова (через пробел)', blank=True)
    category = models.ForeignKey(CategoryRssNews, related_name='category', null=True)
    #models.CharField(max_length=250, verbose_name=u'Ключевые слова (через пробел)', blank=True)
    #models.ForeignKey(CategoryRssNews, related_name='paragraph')
    event_date = models.DateTimeField(verbose_name=u'Дата публикации новости', blank=True)
    preview = models.TextField(verbose_name=u'Анонс новости', blank=True)
    text = models.TextField(verbose_name=u'Полный текст новости', blank=True)
    link = models.CharField(max_length=250, verbose_name=u'Ссылка на оригинальную статью', blank=True, unique=True)



class CompanyNews(models.Model):
    """
    docstring for CompanyNews:
    Класс описывает бизнес логику новостей компании.
    Автором новости является один из членов команды компании.
    """
    class Meta:
        verbose_name = u'Новость компании'
        verbose_name_plural = u'Новоти компании'
        ordering = ('-event_date',)

    author = models.ForeignKey(Team, verbose_name=u'Автор новости')
    name = models.CharField(max_length=250, verbose_name=u'Заголовок новости', blank=True)
    keywords = models.CharField(max_length=250, verbose_name=u'Ключевые слова (через пробел)', blank=True)
    event_date = models.DateField(verbose_name=u'Дата публикации новости', blank=True, default=date.today())
    preview = models.CharField(max_length=385, verbose_name=u'Анонс новости компании. Длина: не более 385 символов', blank=True)
    text = tinymce_models.HTMLField(verbose_name=u'Полный текст новости', blank=True)
    url = models.CharField(max_length=250, verbose_name=u'Относительный путь', blank=True)
    main = models.BooleanField(default=False, verbose_name=u'Присутствие на главной странице')
    image = models.ImageField(verbose_name=u'Изображение', upload_to='photos/company_news', blank=True, null=True)
    text_image = models.TextField(verbose_name=u'Описание изображения', blank=True)
    # weight = models.IntegerField(verbose_name = 'Порядковый номер (сортировка)', blank=True)
    def image_count(self):
        if self.image:
            return 1
        else:
            return 0

class CompanyNewsImage(models.Model):
    """
    docstring for CompanyNewsImage:
    Класс описывает бизнес логику изображений для новостей компании.
    Каждое изображение должно быть связанно с одной новостью.
    Поле "format" отвечает за  формат изображения
    и, следовательно, за область в которую данное изображение будет выводиться.
    """
    class Meta:
        verbose_name = u'Изображение к новости'
        verbose_name_plural = u"Изображения к новости"

    company_news = models.ForeignKey(CompanyNews, related_name='news_image', blank=True)
    image = models.ImageField(verbose_name=u'Изображение', upload_to='photos/company_news', blank=True)
    format = models.BooleanField(default=False, verbose_name=u'Формат изображения')
    text_image = models.TextField(verbose_name=u'Описание изображения', blank=True)

class Tag(models.Model):
    class Meta:
        verbose_name = u'Ключевое слово'
        verbose_name_plural = u'Ключевые слова'

    tag = models.CharField(max_length=250, verbose_name=u'Ключевое слово', blank=True)
    company_news = models.ManyToManyField(CompanyNews, verbose_name=u'Новость компании')