# -*- coding: utf-8 -*-

import models

from AltLan.functions import get_paginator, \
    context_single_functions_order_by_event_date, \
    context_single_functions_order_by_order


# Create your views here.

class RssNewsMixin(object):

    def get_rss_news_all(self):
        rss_news_all = models.RssNews.objects.all().order_by("-event_date")
        return rss_news_all

    def get_categories_rss_news(self):
        categories = models.CategoryRssNews.objects.filter(visible=True).values('id', 'name')
        return categories

    def get_rss_news_context(self):
        pass


class MainRssNewsMixin(RssNewsMixin):

    def get_rss_news_context(self):
            rss_news = self.get_rss_news_all()[:6]
            len_rss_news = len(rss_news)//2
            rss_news_left = rss_news[:len_rss_news]
            rss_news_right = rss_news[len_rss_news:]

            context = {
                "rss_news_left": rss_news_left,
                "rss_news_right": rss_news_right,
            }
            self.context.update(context)

class AllRssNewsMixin(RssNewsMixin):

    def get_rss_news_context(self, page_id=1, pagination=10, *args):
        rss_news_all = get_paginator(self.get_rss_news_all(), page_id, pagination)

        categories = self.get_categories_rss_news()
        context = {"page_context": rss_news_all,
                   'categories':categories}
        self.context.update(context)

class SingleRssNewsMixin(RssNewsMixin):

    def get_single_news(self, page_id):
        try: self.single_news
        except: self.single_news = self.get_rss_news_all().filter(pk=page_id)
        context = {"single_news": self.single_news}
        self.context.update(context)

    def get_single_news_category(self):
        category = self.single_news.values_list('category')#.category #
        return category

    def get_single_news_id(self):
        id = self.single_news.values_list('id')#.id#
        return id

    def get_news_exclude_item(self):
        page_id = self.get_single_news_id()
        news_exclude_item = self.get_rss_news_all().exclude(pk=page_id).order_by("-event_date")
        return news_exclude_item

    def get_rss_single_news_category_last_5(self):
        single_news_category = self.get_single_news_category()
        rss_category_last_5 = self.get_news_exclude_item().filter(category=single_news_category)[:5]
        context = {"rss_category_last_5": rss_category_last_5}
        self.context.update(context)
        return rss_category_last_5

    def get_rss_last_5(self):
        id_rss_category_last_5 = self.get_rss_single_news_category_last_5().values_list('id')
        rss_last_5 = self.get_news_exclude_item().exclude(pk__in=id_rss_category_last_5)[:5]
        context = {"rss_last_5": rss_last_5}
        self.context.update(context)
        #return rss_last_5

    def get_rss_news_context(self, page_id=1):
        self.get_single_news(page_id)
        self.get_rss_last_5()
        self.get_rss_single_news_category_last_5()


class RssNewsByCategoryMixin(RssNewsMixin):

    def get_rss_news_context(self, page_id=1, pagination=10, category_id=1):
        rss_news_all = get_paginator(self.get_rss_news_all().filter(category_id=category_id)
                                                      ,page_id, pagination)

        categories = self.get_categories_rss_news()
        context = {"page_context": rss_news_all,
                   'categories':categories}
        self.context.update(context)


class CompanyNews(object):

    def get_company_news_all(self):
        company_news_all = models.CompanyNews.objects.all().order_by("-event_date")
        return company_news_all


class MainCompanyNewsMixin(CompanyNews):

    def get_company_news_context(self):
        company_news_context = self.get_company_news_all()[:3]
        context= {'company_news_context':company_news_context,}
        self.context.update(context)

class AllCompanyNewsMixin(CompanyNews):
    def get_company_news_context(self, page_id=1, pagination=10):
        company_news_context = get_paginator(self.get_company_news_all(),page_id, pagination)
        context= {'page_context':company_news_context,}
        self.context.update(context)

class SingleCompanyNewsMixin(CompanyNews):
    def get_company_news_context(self, page_id=1):

        context_single, context_next, context_previous = context_single_functions_order_by_event_date(self.get_company_news_all(), page_id)
        context_images = context_single.get().news_image.all() or context_single
        image_count = context_images.count() or context_images.image_count()
        context = {'page_context':context_single,
                'next_context':context_next,
                'previous_context':context_previous,
                'context_images':context_images,
                'image_count':image_count}
        self.context.update(context)



