#!/usr/bin/python
# -*- coding: utf-8 -*-

from math import ceil

from django.http import request
from django.views.generic import TemplateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, Page

# Import mixins
from company_info.views import MainPartnerMixin, MainTeamMixin, MainTeamTextMixin, AllPartnerMixin, SinglePartnerMixin
from site_components.views import HeaderFooterMixin, SiteDivisionMixin, MainHeaderFooterMixin
from news.views import MainRssNewsMixin,     AllRssNewsMixin,     SingleRssNewsMixin, RssNewsByCategoryMixin, \
                       MainCompanyNewsMixin, AllCompanyNewsMixin, SingleCompanyNewsMixin
from services.views import MainProductsMixin, MainProjectsMixin, ProjectMixin, ProductMixin,\
                           ProductParagraphMixin, ProjectParagraphMixin


# Import models
from company_info.models import ContactInfo, Partner, Team
from news.models import CompanyNews, RssNews, CategoryRssNews
from services.models import Project, Product, ProductParagraph, ProjectParagraph

#from functions import context_single_functions_order_by_event_date, context_single_functions_order_by_order

class IndexView(MainPartnerMixin, MainTeamMixin, MainRssNewsMixin,
                MainCompanyNewsMixin, MainProjectsMixin, MainProductsMixin, MainTeamTextMixin,
                MainHeaderFooterMixin, TemplateView):
    def __init__(self):
        self.context = {}

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        self.get_main_header_footer_context()
        self.get_projects_context()
        self.get_rss_news_context()
        self.get_company_news_context()
        self.get_products_context()
        self.get_partners_context()
        self.get_team_context()
        self.get_team_text()
        return self.context



class SingleRssNewsView(SingleRssNewsMixin, HeaderFooterMixin, TemplateView):
    template_name = 'industry_news_item.html'

    def get_context_data(self, page_id=1, **kwargs):
        self.get_header_footer_context()
        self.get_rss_news_context(page_id)
        return self.context


class RssNewsByCategoryView(RssNewsByCategoryMixin, HeaderFooterMixin, TemplateView):
    template_name = 'industry_news.html'

    def get_context_data(self, category_id = 1, page_id=1, **kwargs):
        category_id = category_id or 1
        page_id =  page_id or 1
        context = {'page_name': u'Новости индустрии',
                   'page_url':'/industry_news/category/{}/'.format(category_id)}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_rss_news_context(page_id, pagination=10, category_id=category_id)

        return self.context


class AllRssNewsView(HeaderFooterMixin, AllRssNewsMixin, SiteDivisionMixin, TemplateView):
    template_name = 'industry_news.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'industry_news'

        context = {'page_name': u'Новости индустрии',
                   'page_url':'/industry_news/'}
        self.context.update(context)

        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_rss_news_context(page_id, pagination=10)
        return self.context

class AllCompanyNewsView(AllCompanyNewsMixin, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'company_news.html'

    def get_context_data(self, page_id):
        page_id =  page_id or 1

        mark = 'company_news'
        context = {'page_name': u'Новости компании',
                   'page_url':'/company_news/'}
        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_company_news_context(page_id=page_id, pagination=10)
        self.context.update(context)
        return self.context


class SingleCompanyNewsView(SingleCompanyNewsMixin, HeaderFooterMixin, TemplateView):
    template_name = 'company_news_item.html'

    def get_context_data(self, page_id=1, **kwargs):
        self.get_header_footer_context()
        self.get_company_news_context(page_id)
        context = {'page_name':u'Новость компании',
                   'page_url':'/company_news_item/'}
        self.context.update(context)

        return self.context


class AllPartnerView(AllPartnerMixin, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'partners.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'partners'
        context = {'page_name': u'Наши партнеры',
                   'page_url':'/partners/'}
        self.context.update(context)

        self.get_division_context(mark)
        self.get_header_footer_context()
        self.get_partners_context(page_id, pagination=10)
        return self.context


class SinglePartnerView(SinglePartnerMixin, HeaderFooterMixin, TemplateView):
    template_name = 'partner.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1

        context = {'page_name': u'Партнер компании',
                   'page_url':'/partner/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_partners_context(page_id)
        return self.context


class AllProjectView(ProjectMixin, HeaderFooterMixin,SiteDivisionMixin, TemplateView):
    template_name = 'projects.html'

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'projects'
        self.get_division_context(mark)
        context = {'page_name':  u'Проекты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_all_context(page_id, pagination=10)
        return self.context

class SingleProjectView(ProjectParagraphMixin,ProjectMixin, HeaderFooterMixin, TemplateView):
    template_name = 'single_service.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1
        context = {'page_name': u'Проекты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_single_context(page_id)
        self.get_paragraphs_context(page_id)

        return self.context


class AllProductView(ProductMixin, HeaderFooterMixin, SiteDivisionMixin, TemplateView):
    template_name = 'products.html' 

    def get_context_data(self, page_id=1, **kwargs):
        mark = 'products'
        self.get_division_context(mark)
        context = {'page_name': u'Продукты компании',
                   'page_url':'/projects/'}
        self.context.update(context)

        self.get_header_footer_context()
        self.get_all_context(page_id, pagination=10)
        return self.context


class SingleProductView(ProductParagraphMixin,ProductMixin,HeaderFooterMixin, TemplateView):
    template_name = 'single_service.html'

    def get_context_data(self, page_id=1, **kwargs):
        page_id = page_id or 1
        self.get_header_footer_context()
        context = {'page_name': u'Продукт компании',
                   'page_url':'/product/'}
        self.context.update(context)
        self.get_single_context(page_id)
        self.get_paragraphs_context(page_id)
        return self.context


class AboutView(HeaderFooterMixin, TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        self.get_header_footer_context()
        self.context['page_name'] = u'О компинии'
        self.context['contact_info'] = ContactInfo.objects.all()
        self.context['team'] = Team.objects.all()
        return self.context
