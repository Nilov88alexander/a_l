from django.contrib import admin
import models

# Register your models here.

class TeamAdmin(admin.ModelAdmin):
    list_display = ('surname', 'order', 'main', 'post', 'phone', 'email',)
    list_filter = ('post','order',)
    search_fields = ['surname', ]
    list_editable = ( 'order', 'main', 'post', 'phone', 'email',)

class ContactInfoAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email',)
    list_editable = ('phone', 'email',)


class  PartnerAdmin(admin.ModelAdmin):
    list_display = ('name','order', 'phone', 'email',)
    search_fields = ['name', ]
    list_editable = ('order', 'phone', 'email',)

admin.site.register(models.ContactInfo, ContactInfoAdmin)
admin.site.register(models.Team, TeamAdmin)
admin.site.register(models.Partner, PartnerAdmin)