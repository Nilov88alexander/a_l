# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='email',
            field=models.EmailField(default=b'', max_length=75, verbose_name='Email \u0430\u0434\u0440\u0435\u0441'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='partner',
            name='phone',
            field=models.CharField(default=b'', max_length=250, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=True,
        ),
    ]
