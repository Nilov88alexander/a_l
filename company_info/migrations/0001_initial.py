# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='\u041a\u043e\u043c\u043f\u0430\u043d\u0438\u044f \u0417\u0410\u041e \xab\u0410\u043b\u044c\u0442-\u041b\u0430\u043d\xbb', max_length=250, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438')),
                ('address', models.CharField(default='\u0417\u0432\u0435\u0437\u0434\u043d\u044b\u0439 \u0431\u0443\u043b\u044c\u0432\u0430\u0440, \u0434\u043e\u043c 21, \u0441\u0442\u0440\u043e\u0435\u043d\u0438\u0435 1\u043e\u0444\u0438\u0441 711', max_length=250, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('email', models.EmailField(default=b'info@alt-lan.ru', max_length=75, verbose_name='Email \u0430\u0434\u0440\u0435\u0441')),
                ('phone', models.CharField(default=b'', max_length=250, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
            ],
            options={
                'verbose_name': '\u041a\u043e\u043d\u0442\u0430\u043a\u0442',
                'verbose_name_plural': '\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250, verbose_name='\u0418\u043c\u044f \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430')),
                ('text', tinymce.models.HTMLField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430', blank=True)),
                ('preview', tinymce.models.HTMLField(verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432')),
                ('url', models.CharField(max_length=250, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0443\u0442\u044c')),
                ('logo_mono', models.ImageField(upload_to=b'photos', verbose_name='\u041c\u043e\u043d\u043e\u0445\u0440\u043e\u043c\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043b\u043e\u0433\u043e\u0442\u0438\u043f\u0430')),
                ('colored_mono', models.ImageField(upload_to=b'photos', verbose_name='\u0426\u0432\u0435\u0442\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043b\u043e\u0433\u043e\u0442\u0438\u043f\u0430')),
            ],
            options={
                'verbose_name': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440',
                'verbose_name_plural': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post', models.CharField(default=b'', max_length=250, verbose_name='\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c')),
                ('name', models.CharField(default=b'', max_length=250, verbose_name='\u0418\u043c\u044f')),
                ('surname', models.CharField(default=b'', max_length=250, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('phone', models.CharField(default=b'', max_length=250, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(max_length=75)),
                ('image', models.ImageField(upload_to=b'photos', verbose_name='\u0424\u043e\u0442\u043e')),
            ],
            options={
                'verbose_name': '\u0427\u043b\u0435\u043d \u043a\u043e\u043c\u0430\u043d\u0434\u044b',
                'verbose_name_plural': '\u0427\u043b\u0435\u043d\u044b \u043a\u043e\u043c\u0430\u043d\u0434\u044b',
            },
            bases=(models.Model,),
        ),
    ]
