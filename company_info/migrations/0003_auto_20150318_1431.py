# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0002_auto_20150317_1655'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactinfo',
            name='slogan',
            field=tinymce.models.HTMLField(verbose_name='\u0421\u043b\u043e\u0433\u0430\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contactinfo',
            name='team_text',
            field=tinymce.models.HTMLField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u0430\u043d\u0434\u044b', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='partner',
            name='main',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u0432\u043e\u0434 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u0443\u044e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='team',
            name='main',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u0432\u043e\u0434 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u0443\u044e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443'),
            preserve_default=True,
        ),
    ]
