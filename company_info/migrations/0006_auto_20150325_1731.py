# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0005_auto_20150323_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='email',
            field=models.EmailField(default=b'', max_length=75, verbose_name='Email \u0430\u0434\u0440\u0435\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='logo_colored',
            field=models.ImageField(upload_to=b'photos/partner', verbose_name='\u0426\u0432\u0435\u0442\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043b\u043e\u0433\u043e\u0442\u0438\u043f\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='logo_mono',
            field=models.ImageField(upload_to=b'photos/partner', verbose_name='\u041c\u043e\u043d\u043e\u0445\u0440\u043e\u043c\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043b\u043e\u0433\u043e\u0442\u0438\u043f\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='name',
            field=models.CharField(max_length=250, verbose_name='\u0418\u043c\u044f \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='phone',
            field=models.CharField(default=b'', max_length=250, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='preview',
            field=tinymce.models.HTMLField(verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='url',
            field=models.CharField(max_length=250, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0443\u0442\u044c', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='team',
            name='image',
            field=models.ImageField(upload_to=b'photos/team', verbose_name='\u0424\u043e\u0442\u043e'),
            preserve_default=True,
        ),
    ]
