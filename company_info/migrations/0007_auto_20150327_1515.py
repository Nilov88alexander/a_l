# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0006_auto_20150325_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactinfo',
            name='slogan',
            field=models.TextField(verbose_name='\u0421\u043b\u043e\u0433\u0430\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contactinfo',
            name='team_text',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u0430\u043d\u0434\u044b', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='preview',
            field=models.CharField(max_length=385, verbose_name='\u0410\u043d\u043e\u043d\u0441 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='partner',
            name='text',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u0430', blank=True),
            preserve_default=True,
        ),
    ]
