# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('company_info', '0004_auto_20150318_2353'),
    ]

    operations = [
        migrations.RenameField(
            model_name='partner',
            old_name='colored_mono',
            new_name='logo_colored',
        ),
    ]
