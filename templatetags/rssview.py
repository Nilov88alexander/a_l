#!/usr/bin/python
# -*- coding: utf-8 -*-
from news.models import RssNews

__author__ = 'nilov'

from django import template

register = template.Library()
import datetime
import feedparser
import html5lib
from html5lib import treebuilders
import lxml.html as html
from bs4 import BeautifulSoup
from urllib2 import urlopen


last_pk = int(RssNews.objects.values("pk").last())


@register.inclusion_tag('Wd5PageApp/rssfeed.djhtml')
def pull_feed(feed_url, posts_to_show=5):
    try:
        feed = feedparser.parse(feed_url)
        posts = []
        for i in range(posts_to_show):
            pub_date = feed['entries'][i].updated_parsed
            published = datetime.date(pub_date[0], pub_date[1], pub_date[2])
            posts.append({
                'title': feed['entries'][i].title,
                'summary': (feed['entries'][i].summary),
                'link': feed['entries'][i].link,
                'date': published,
                'category': feed['entries'][i].category,
            })
    except:
        pass
    return {'posts': posts}
    #print posts


if __name__ == '__main__':
    category_list = [u'Облачные технологии', u'Информатизация', u'Новости CNews', u'Интеграция', u'Бизнес', u'Наука',
                     u'Техника', u'Софт', u'Игры']
    link_list = []
    # http://www.cnews.ru/news.xml
    try:
        pull_feed_list = pull_feed(u'http://www.cnews.ru/news.xml', 10)
    except:
        pull_feed_list = {'posts':"posts"}
    # print pull_feed_list['posts'][0].keys()
    print '['
    for i in pull_feed_list['posts']:
        if unicode(i['category']) in category_list:
            last_pk = last_pk +1
            print '{"model": "news.RssNews",'
            print '"pk":', last_pk, '",'
            print '"fields": {'
            print '"title": "', i['title'].encode('utf-8'), '",'
            print '"link": "', i['link'], '",'

            print '"preview": "', i['summary'].encode('utf-8'), '",'
            print '"category": "', i['category'].encode('utf-8'), '",'
            url = unicode(i['link'])
            html_doc = urlopen(url).read()
            soup = BeautifulSoup(html_doc).find('div', 'NewsBody _ga1_on_').select('p')
            print '"text": "', str(soup).replace('</p>, <p>','').replace('[','').replace(']','').replace('Читайте также:','').replace('"', "'"), '"}'
            print '},'
    print ']'







            #print (str(soup).replace('</p>, <p>','').replace('</p>','').replace(']','').replace('<span>...</span></a>','\n')
        #.replace('<span>...','').replace('</span>','').replace('<a>...','').replace('</a>',''))
    #print link_list
    #http://python-3.ru/page/parser-html-python
    #for url in link_list:
    #    html_doc = urlopen(url).read()
    #    soup = BeautifulSoup(html_doc).find('div', 'NewsBody _ga1_on_').select('p')
    #    parser = html5lib.HTMLParser(tree=treebuilders.getTreeBuilder("etree"))
        #soup = parser.parse(html_doc)
        #soup = html5lib.parse.a feed(html_doc)
    #    print soup
        #print (str(soup).replace('</p>, <p>','').replace('</p>','').replace(']','').replace('<span>...</span></a>','\n')
        #.replace('<span>...','').replace('</span>','').replace('<a>...','').replace('</a>',''))