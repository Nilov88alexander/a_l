# -*- coding: utf-8 -*-

from django.contrib import admin
import models



class ProjectParagraphInline(admin.StackedInline):
    model = models.ProjectParagraph
    extra = 1


class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'order', 'image_tag')
    list_editable = ('order',)
    inlines = (ProjectParagraphInline, )
    list_filter = ('keywords',)
    search_fields = ['name', ]

    def image_tag(self, obj):
        return '<img src="%s" height=200 width=200>' % obj.image.url
    image_tag.allow_tags = True

class ProductParagraphInline(admin.StackedInline): # вывод в линию:(admin.TabularInline):
    model = models.ProductParagraph
    list_display = ('order',)
    extra = 1

class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'order', 'image_tag')
    list_editable = ('order',)
    inlines = [ProductParagraphInline, ]
    list_filter = ('keywords',)
    search_fields = ['name', ]
    def image_tag(self, obj):
        return '<img src="%s" height=200 width=200>' % obj.image.url
    image_tag.allow_tags = True


admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.Product, ProductAdmin)