# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0011_auto_20150401_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productparagraph',
            name='product',
            field=models.ForeignKey(related_name='paragraph', to='services.Product'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='projectparagraph',
            name='project',
            field=models.ForeignKey(related_name='paragraph', to='services.Project'),
            preserve_default=True,
        ),
    ]
