# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0003_auto_20150318_1431'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectparagraph',
            name='text_image',
            field=tinymce.models.HTMLField(verbose_name='\u041f\u0430\u0440\u0430\u0433\u0440\u0430\u0444 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430/\u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
            preserve_default=True,
        ),
    ]
