# -*- coding: utf-8 -*-

import models

from AltLan.functions import get_paginator, \
    context_single_functions_order_by_order
from math import ceil
# Create your views here.


class ServicesMixin():

    def get_items_all(self):
        return "text"

    def get_all_context(self,page_id, pagination):
        context = get_paginator(self.get_items_all(), page_id, pagination)
        context= {'page_context':context,}
        self.context.update(context)

    def get_single_context(self, page_id):

        context_single, context_next, context_previous = context_single_functions_order_by_order(self.get_items_all(), page_id)

        context = {'page_context':context_single,
                'next_context':context_next,
                'previous_context':context_previous}
        self.context.update(context)

class ProductMixin(ServicesMixin):

    def get_items_all(self):
        items_all = models.Product.objects.all().order_by('order')
        return items_all

class MainProductsMixin(ProductMixin):

    def get_products_context(self):
        context = models.Product.objects.all().order_by('order')[:3]
        print context
        context= {'product_context':context}
        self.context.update(context)


class ProjectMixin(ServicesMixin):

    def get_items_all(self):
        items_all = models.Project.objects.all().order_by('order')
        return items_all

class MainProjectsMixin(ProjectMixin):

    def get_projects_context(self):
        context = models.Project.objects.all().order_by('order')[:3]
        context= {'product_context':context}
        self.context.update(context)




class ParagraphMixin(object):

    def get_paragraphs_all(self):
        pass

    def get_paragraphs_context(self,page_id):
        context_paragraphs = self.get_paragraphs_all(page_id)
        count_paragraph = ceil(context_paragraphs.count()/2.0)
        context = {'context_col1':context_paragraphs[:count_paragraph],
                   'context_col2':context_paragraphs[count_paragraph:]}
        self.context.update(context)


class ProjectParagraphMixin(ParagraphMixin):
    def get_paragraphs_all(self, page_id):
        return models.ProjectParagraph.objects.filter(project__id=page_id)

class ProductParagraphMixin(ParagraphMixin):
    def get_paragraphs_all(self, page_id):
        return models.ProductParagraph.objects.filter(product__id=page_id)