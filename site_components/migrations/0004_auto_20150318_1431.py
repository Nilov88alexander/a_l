# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_components', '0003_auto_20150317_1749'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='menu',
            options={'ordering': ('order',), 'verbose_name': '\u041f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e', 'verbose_name_plural': '\u041f\u0443\u043d\u043a\u0442\u044b \u043c\u0435\u043d\u044e'},
        ),
        migrations.AlterModelOptions(
            name='slider',
            options={'ordering': ('order',), 'verbose_name': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440', 'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440\u044b'},
        ),
        migrations.RenameField(
            model_name='menu',
            old_name='weight',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='slider',
            old_name='weight',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='slider',
            old_name='caption',
            new_name='title',
        ),
    ]
