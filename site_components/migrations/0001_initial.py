# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('url', models.CharField(max_length=250, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043f\u0443\u0442\u044c')),
                ('weight', models.IntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440')),
            ],
            options={
                'verbose_name': '\u041f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e',
                'verbose_name_plural': '\u041f\u0443\u043d\u043a\u0442\u044b \u043c\u0435\u043d\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('caption', models.CharField(max_length=50, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('text', models.CharField(max_length=385, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430. \u0414\u043b\u0438\u043d\u0430: \u043d\u0435 \u0431\u043e\u043b\u0435\u0435 385 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432')),
                ('image', models.ImageField(upload_to=b'photos', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0434\u043b\u044f \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430', blank=True)),
                ('weight', models.IntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043a\u043e\u0432\u044b\u0439 \u043d\u043e\u043c\u0435\u0440')),
            ],
            options={
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440',
                'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
    ]
